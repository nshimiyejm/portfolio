from django.shortcuts import render, get_object_or_404
from .models import Job

# Create your views here.
def profile(request):
    # this code will pull all the items in job and gets stored in the jobs variable
    jobs = Job.objects

    # pass forward a dictionary that will be used to display the content on the webpage
    return render(request, 'jobs/index.html', {'jobs': jobs})

def details(request, job_id):
    job_detail = get_object_or_404(Job, pk=job_id)
    return render(request, 'jobs/detail.html', {'job':job_detail})