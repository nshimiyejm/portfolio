from django.db import models

# Create your models here.

class Job(models.Model): 
    # Holds an image
    image = models.ImageField(upload_to='images/') # upload this image and save it to a folder named images 

    # Holds the description
    summary = models.CharField(max_length = 250)

    def __str__(self):
        return self.summary